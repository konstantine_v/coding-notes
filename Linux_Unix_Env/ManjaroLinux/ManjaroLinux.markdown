# Notes for Pacman commands

These are my local notes to remember my commonly used pacman commands on manjaro. you can see the rest of the commands at: https://wiki.manjaro.org/index.php?title=Pacman_Overview

Always run with `sudo`, duh

Install:
`pacman -S <package>`
Better practice is to use `pacman -Syu <package>`, so you check all packages before download. If the internet is slow or you're in a hurry I guess use `-S`

Update all system packages and databases:
`pacman -Syu`

Search the pacman repos online:
`pacman -Ss <packagename>`

Search local packages:
`pacman -Q <packagename>`

Remove package:
`pacman -R <package>`

Remove package, but leave deps:
`pacman -Rsu <package>`

To clear the cache of downloads that have already been installed, enter the following command:
`sudo pacman -Sc`

Otherwise, to clear the cache completely, enter the following command (and use with care):
`sudo pacman -Scc`

A safer way to remove old package cache files is to remove all packages except for the latest three package versions using paccache:
`paccache -rvk3`

============================================================================

### Enabling Color Output
By default, pacman's output is monochrome but enabling colored output can make the output easier to read if your terminal supports colors. This can be enabled by uncommenting ort adding the following line to the file

`Color`


### Showing PacMan Eating Power Pills
If you are bored of simply watching lines of hashes while downloading software packages in the terminal, why not change the progress bar to Pacman eating power pills instead? To enable this, simply add the line:

`ILoveCandy`
