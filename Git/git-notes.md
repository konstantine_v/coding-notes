# Git Notes

## Git

- Staging for commit
- `git add .`
- Commiting the latest version
- `git commit -m “”`
- pushing the commit
- `git push origin <development branch>`

### first time pushing to gh-pages

Pushes to both `master` and `gh-pages` branches at the same time. This creates the gh-pages branch, then the second line accesses both branches and pushes all the commits on both

You typically would do this if you're working on a project and you don’t care about separate branches but instead would rather just have your project hosted
```
git branch gh-pages
git fetch . master:gh-pages && git push origin --all
```

### after first time

All you need is the one line, so you can continue to work on the master branch, but push whenever you want to publish to the gh-pages with this code
`git fetch . master:gh-pages && git push origin --all`

### Pushing individual branches

To set up a separate branch you would run `git branch <branch name>`
If you want to change to the branch you created and work on that one, run `git checkout <branch name>` and then push as you would normally. This is useful when you’re working on multiple branches with with different features in mind (ie You want to integrate the Stripe API into your project and don’t want to disrupt the main dev branch. Or you’re working on a project with a friend and you make a separate branch to solve a single issue)

### Don't forget (when forking)

`git pull upstream <develop>` use this to update to the latest development ver.
Make sure to solve any conflicts that git may have. Might require making a new branch if there’s conflict with the source.

### Use in an emergency

Very rare that you'll have to use this, only use it on your own repositories. I recommend not using this at all and figuring out what merge conflicts there are.

`git push origin --all --force`

⋅⋅⋅Can break development, you have have been warned.
Don't use if working with others!!!

## Hub

`hub` is an awesome tool that can really make the git process a lot easier
- If you already have Homebrew installed on MacOS than install using `brew install hub`
- If you don’t have Homebrew installed (Which I’d recommend you do if you’re a developer) than follow the instructions [here](https://brew.sh/) and then run `brew install hub`

Creating a repo on github is as easy as `hub create`

So when you’re working on a new project, now you can setup git and the repo with two lines, here's an example:
```
git init
hub create
```

## Heroku Projects

Pushing to a Heroku server?
`git push heroku master`


## Git-FTP

Install `git-ftp` package 
Useful for launching static sites to ftp without having to manually upload each file via cPanel or whatever other file system on server. Makes it easy to make changes quick to a live site straight from your command line.

### Setup

First Time setup

```
git config git-ftp.url <ftp.example.net> && git config git-ftp.user <ftp-user> && git config git-ftp.password <secr3t>
```
Fill out the portions in the “<>”.
Make sure to go into your `.git>config` and make sure the data is correct before pushing anything.


### Initial Push

`git ftp init`
Your initial Push. This uploads all the files.

### Upload all files

`git ftp push`
Uploads all the files that have been stages to commit and are ready to push.

### Or if the files are already there

`git ftp catchup`
For some hosting services, you might not want to do this option and just do git ftp push.

### Work and deploy

```
echo "new content" >> index.txt
git commit index.txt -m "Add new content”
git ftp push
```
For testing purposes.
