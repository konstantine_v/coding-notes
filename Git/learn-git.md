# Git Notes

## Intro

Alright, so I'll go over the why and how of git, as well as some of the command-line (Terminal).

## Prerequisites

You'll need some stuff before you start working with git. Some of these might seem obvious, but I'll go over everything you need with links and commands for each thing. So lets begin.

⋅⋅⋅First and foremost, make sure your OS is up to date. MacOS Sierra and Windows10 preferably.

- 'command-line tools' - The command line tools for MacOS. Copy and paste `xcode-select --install` into the terminal. This should bring up a prompt to install XCode's command line tools. This is what will allow you to use git and all the other cool stuff in terminal. for a more in depth discussion on command line tools and what exactly it does, check out <http://osxdaily.com/2014/02/12/install-command-line-tools-mac-os-x/>

- `github` - You'll have to setup a GitHub account to push your code up for people to see. We'll be using this to grade your assignments as well, but this also serves as your programming portfolio.

- `git` - You might already have git installed if you installed the GitHub Desktop client in the intro. If you didn't here's the link to do so, even though we wont be using the Desktop client, it can still come in handy if you're ever stuck <https://desktop.github.com/>.

- `hub` - This is one of my personal favorites for git, and it's not necessary but it'll save a lot of time setting up a new repository and whatnot. Follow the instructions on the website to install (requires brew <http://brew.sh/>). <https://hub.github.com/>.

- `ssh key` - Your SSH key is the bridge between your computer and your github account, more specifically, it's your computer's fingerprint that GitHub will see and know that it's you and not someone else pushing stuff to your GitHub account. Here's what you need to do:

⋅⋅⋅Go to root `cd ~`

⋅⋅⋅List files in .ssh directory `ls -al !/.ssh`

⋅⋅⋅Generate ssh key `ssh-keygen -t rsa -b 4096 "example@gmail.com"`

⋅⋅⋅Press `enter` for all the info (for default quick setup)

⋅⋅⋅The command line will output a fingerprint for your computer (SSH Key)

⋅⋅⋅Add your key to the ssh-agent `eval "$(ssh-agent -s)"` `ssh-add ~/.ssh/id_rsa`

⋅⋅⋅Now to copy the SSH key, run `pbcopy < ~/.ssh/id_rsa.pub`. Then go through the instructions on <https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/>

⋅⋅⋅Now test your SSH `ssh -T git@github.com`

⋅⋅⋅And now you should be ready to go!

### The Git Process

So what Git does is it saves a version of the project your working on, more specifically, it saves all the changes you make and helps you and perhaps the team you're working with, be able to know who made a change, what it does, and when the change was made in case an older version needs to be used due to a bug. Git also allows you to pull down older versions and use those, comment on code, and know what you're code is doing. Git can do so much, and it makes developing sites and apps so much easier.

⋅⋅⋅Let me run through the process of setting up git on a project.

- `mkdir <project>` - This creates a file called project.
- `cd <project>` - This brings you into the folder you created.
- `git init` - This initializes git inside the repository that you've created. It adds a folder called .git, which you wont be able to see when running the `ls` command in terminal.
- `hub create` - This is a Git command that creates a repository on GitHub for the folder you made and initialized. Refer to earlier in the notes how to install the `hub` command in the terminal
- Now from here you can start working on your project!

⋅⋅⋅Let me run through the usual process someone using git would use.

- `git status` - So, you'll notice `git`, this is the terminal command for git, after it are the commands that git recognizes. `status` lists out the status of the current project folder you're working in by seeing what changes have been made compared to the last version. Running this command outputs in the console all the files that have been changed, as well as what to do next for git.
- `git add .` - So now `add`, 'adds' all the files, which means it sets all the files that were changed to be ready for the commit process. `.` is the current repository you're working in which lets git know what files to add to the commit, you can specify what file you'd want by replacing `.` with something like `index.html` for example so only that file is added to the commit, Which can be useful if you want to have a commit be specific to the file that you changed.
- `git commit -m "example"` - Alright, so this one is a little more complex. `commit` takes the staged file(s) that are ready for commit, whenever you 'added' the files earlier via the `add` command it let git know that the changes are ready for commit. `-m` stands for message, so you can add a commit message to know what you changed so you or other people can go through your project and know what was changed and why. `"example"` is the message that you want to write, it has to be within quotation marks or else git wont recognize it and possibly add it's own message (it would look something like: 3ES345GSDGJN43G39).
- `git push origin master` - So this is the final part of the git process where you push the changes to GitHub. `push` is the command that git recognizes to push the code up. `origin` is the where, and it means the origin repository which we would have specified when creating the project. `master` lets the push and origin commands that you want to push to the master branch, the default branch that github creates.
- And that's it, you've pushed up your project up to GitHub.

So you may be wondering after all that why it'd be useful or when you'd use a process like this, so I'll try and give some examples and try to help wrap your mind around this awesome workflow.

So in both software development and web development there's many people working on a single project, and you want to make sure that everyone understands when a change has been made and how to accommodate for those changes. Let's say you're working on a project with your friend, but you don't want your code to interfere with his. What you'd do is, create a new branch and start developing on that, and when the time comes to have the code meet and be ready for production, you can do that easily with a git merge, and if there's any errors you can work through them until everything is working and ready to go. If you want to go to an earlier build to maybe see where a problem was, you can do that also.

This makes the development process so much nicer, and can really help teams work together more efficiently.

There's even more to git than just the git command itself, there's other commands that work with git such as the hub command from earlier. Aside from addons, you can create your own git scripts or custom commands using a shell script to automate your git process, run a custom set of commands within a single command, etc.

Go explore what git has to offer! And if you liked this, make sure to star this repo. If you want to correct any info or change anything, feel free to make a pull request!
