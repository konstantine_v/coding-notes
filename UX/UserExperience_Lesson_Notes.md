## Lesson 1

#### Lesson 1.1
​Taking a user-centered approach as opposed to a business-centered approach would allow a team to think outside of the box and understand the user more. Although it’s important that a product meets the needs of the business, it means nothing if it doesn’t take into account what the users of that product actually want, expect, or need from it.
The potential to using the approach is a closer connection to the users, which garners those users trust, and helps keep people coming back to that business through its’ products. By benefiting the user, it benefits the business.

#### Lesson 1.4
Empathetic Design is, what I think, a way for design teams to understand the user by seeing their problems as more human and relate as well as seeing the user as a human rather than a test subject.
Using empathy in the design process allows a team to better understand the correlation of what people want and what people need. In the role of UX designer, it’s important to understand where the user is coming from, that means asking question to understand and better empathize with the user to tackle problems that are relevant and with meaning.

#### Lesson 1.4
User experience, or more generally speaking, Design, ​isn't just about making the site easier to use but also pleasant to use. It’s about the look, feel, and usability of the product. Design is about simplifying and refining processes to allow users of said design to enjoy their time with that product and/or make their life’s easier.

Quite a few users I’ve interviewed in the past explained that the main point of criticism of a website or application that they are using is the speed in which it takes to load, and amount of clicks it takes to perform an action. These people often said that they would use an app based on what it does as well as how fast it is, over how it looks.


#### Lesson 2.1
The possible ROI for UX Design would be something like the improvement of the layout in a store. In my experience, interviewing users, seeing where they go, how easy they find something in a store can help setup a case to see if there are any improvements to be made. Once you have the data, now you can go in and see where the users may be struggling and hone down on those areas to better improve their experience. A case, such as a user not being able to find an expensive item in an isle easily. Small improvements for situations like these can save companies hundreds in the long run, for example: 8 hours of work (100-250) from an employee interviewing and creating a report can bring in hundreds from previously lost revenue. Since brick and mortar stores are suffering in this day and age, it becomes more important that these companies hone in and improve on the weak links in their chains.








