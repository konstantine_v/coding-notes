Interview questions
1.  If we build a tool that had all the data you look for in external part search websites, what do you think it should have?
    - What do you look for?
    - What can't you find that you wish would be there?
2. What kind of sales requests do you receive? Recurring orders? How do you handle these requests?
3. How does vendor relationship effect price and negotiation?
4. How do you handle BOM requests? Does it effect interaction with vendors (Pricing, Negotiation, etc.)?
5. How do you determine what the best offer is?
6. How do you know when something is in shortage or high demand?

Questions about customer (Relevant only to Traders)
1. How does a customer initially contact you?
2. What kind of information does a customer provide?
3. How many customers are recurring?
4. How do traders build and maintain trust with customers?
5. How many customers request QC Photos? Do they ever inquire about the process?
6. How often do you up-sell parts?
7. What makes a customer come back to us?