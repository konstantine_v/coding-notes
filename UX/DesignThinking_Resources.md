# IDEO: Design Thinking Mindsets (Resources)
A series of videos produced by IDEO.org features a few key mindsets design thinkers should adopt when solving problems. Each mindset is explained by a Design Thinking champion or leader of a company that successfully adopted the Design Thinking process.

Iterate, Iterate, Iterate – by Gaby Brink, Founder of Tomorrow Partners: http://www.designkit.org/mindsets/7

Creative Confidence – by David Kelley, Founder of IDEO: http://www.designkit.org/mindsets/3

Embrace Ambiguity – by Patrice Martin, Creative Director of IDEO.org: http://www.designkit.org/mindsets/5

Optimism – by John Bielenberg, Founder of Future Partners: http://www.designkit.org/mindsets/6

Make It – by Krista Donaldson, CEO of D-Rev: http://www.designkit.org/mindsets/2

Learn From Failure – by Tim Brown, CEO of IDEO: http://www.designkit.org/mindsets/1

Design Thinking: Wicked Problems, Designing Vs. Engineering, Prototyping Your Life by Dave Evans