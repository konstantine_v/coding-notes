var simpleRSSPlugin = (function() {
  var feedsNodes = document.querySelectorAll('[data-rss-feed]');
  var feeds = [].slice.call(feedsNodes);
  for (var i = 0; i < feeds.length; i++) {
    var container = feedsNodes[i];
    var url = container.getAttribute('data-rss-feed');
    var addLink = container.getAttribute('data-rss-link-titles') || 'true';
    var titleWrapper = container.getAttribute('data-rss-title-wrapper') || 'h2';
    var max = container.getAttribute('data-rss-max') || 10;
    var script = document.createElement('script');
    script.src = document.location.protocol + '//api.rss2json.com/v1/api.json?callback=simpleRSSPlugin.handleJSON&rss_url=' + encodeURIComponent(url);
    document.querySelector('head').appendChild(script);
    script.parentNode.removeChild(script);
  }
  function handleJSON(data) {
    if (data.feed && data.items) {
      var docFrag = document.createDocumentFragment();
      for (var i = 0; i < data.items.length; i++) {
        var e = data.items[i];
        var tempNode = document.createElement('div');
        var template = '<' + titleWrapper + '><a href="' + e.link + '">' + e.title + '</a></' + titleWrapper + '>' + e.content;
        if (addLink === 'false') {
          template = '<' + titleWrapper + '>' + e.title + '</' + titleWrapper + '>' + e.content;
        }
        if (i < max) {
          tempNode.innerHTML = template;
          docFrag.appendChild(tempNode);
        }
      }
      container.appendChild(docFrag);
    }
  }
  return {
    handleJSON: handleJSON
  }
})();
