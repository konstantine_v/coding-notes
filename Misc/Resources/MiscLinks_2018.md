# Dev Things:
jquery-ui: `https://snyk.io/vuln/npm:jquery-ui?lh@1.10.4`
jquery: `https://snyk.io/vuln/npm:jquery?lh@1.12.4`
Fake Info: `https://www.fakeaddressgenerator.com/Index/index`
Image SpriteSheet: `https://www.toptal.com/developers/css/sprite-generator`
Data plotting in JS: `http://plottablejs.org/`
Status Page: `https://hackernoon.com/build-a-great-status-page-in-15-minutes-with-no-budget-98257f67aef1`
Android Emulator: `https://www.bluestacks.com/`
Bulma WP: `https://github.com/teamscops/bulmapress`
Mobile Dev: `https://flutter.io/` 

### Some MySQL code:
```UPDATE admin_user SET password = CONCAT(SHA2('xxxxxxxYourNewPassword', 256), ':xxxxxxx:1') WHERE username = 'admin';
UPDATE sm7h_catalog_eav_attribute SET is_filterable = '1', is_visible = '1', is_visible_on_front = '1' WHERE attribute_id = '140';```

Computer Component Part Buying Sites:
https://www.ozdisan.com/integrated-circuits-ics/memory/ic-embedded-memories/AT24C02C-SSHM-T
https://www.ttiasia.com/content/ttiasia/en/apps/part-detail.html?mfrShortname=MUR&partsNumber=GRM155R71H392KA01D&customerPartNumber=&minQty=10000&customerId=
http://www.futureelectronics.com/en/technologies/semiconductors/analog/amplifiers/general-purpose/Pages/2399004-NCV2904DR2G.aspx?IM=0
http://www.efreewind.cn/goods.php?id=4743

# Trade Shows:
`https://www.alibaba.com/tradeshows/calendar?spm=a2700.8293689.2018taBelt.1.2ce214beaQuinj&tracelog=20190122_ICBU_PC_HOME_DAILY_BELT_ICBU_PC_HOME_DAILY_BELT_NORMAL_T`
Install Windows 9.5: `https://linustechtips.com/main/topic/748494-windows-95-install-guide/`
Retro Game Programming: `https://8bitworkshop.com/`
Dashcam: `https://www.alibaba.com/product-detail/2018-Full-HD-mini-CCTV-wifi_60788597283.html?spm=a2700.industrynonetrends.offer-list.6.4ccc2098uD0Qwu`
`https://securitycheckli.st`
`http://www.searsarchives.com/catalogs/`
`https://reactos.org/wiki/Games_ROS_Testing`
`https://triplelift.com/` The future of ads
`https://github.com/sindresorhus/awesome#gaming`
`https://www.delorean.com`
`https://support.libsyn.com/faqs/libsyn/`

# Learning:
Python Course: `https://www.youtube.com/watch?v=rfscVS0vtbw`
`https://uxdesign.cc/the-8-second-rule-ef2a60c5813c`
Hashes in Python: `https://www.youtube.com/watch?v=M0wqrfKiSNA`
PyQT5:  `https://likegeeks.com/pyqt5-tutorial/`
Python security: `https://nitratine.net/blog/post/encryption-and-decryption-in-python/`
Python DB: `https://stackoverflow.com/questions/3710263/how-do-i-create-a-csv-file-from-database-in-python#`
Python Networking: `https://www.howtogeek.com/108511/how-to-work-with-the-network-from-the-linux-terminal-11-commands-you-need-to-know/`
Writing Resource: `https://writetogether.space/?utm_source=densediscovery&utm_medium=email&utm_campaign=newsletter`
Affinity Photo: `https://www.youtube.com/watch?v=gUlqoOEkP_o`
Cursed Books: `https://en.wikipedia.org/wiki/Deuterocanonical_books1` `https://en.wikipedia.org/wiki/Codex_Gigas`
`https://www.codecademy.com/learn/learn-rails`

# FrontEnd Development:
`https://mithril.js.org/framework-comparison.html`
`https://mithril.js.org/simple-application.html`
`https://scrimba.com/playlist/playlist-34`
Backend development: Ruby
`https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one`


# Design Inspiration:
`https://www.behance.net/gallery/73746099/Logotype-Collection-?tracking_source=curated_galleries_list`
`https://www.behance.net/gallery/72452975/Backup-Days?tracking_source=curated_galleries_list`