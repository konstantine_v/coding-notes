# Quick notes on `vim`

The mouse is overrated and bloated. l;earn keybindings and make your programs work for you.

Copy and paste a line: `yy` > `p`
Cut and paste a whole line: `V` > `p`
To delete a line press `dd`, to delete up until the end of the line do `d$`, to delete five lines (downward) do `d5d`

# NERDTree
Show hidden files in tree, `Shift + i`
To jump betweem tab and editor, `CRTL + w + w`


